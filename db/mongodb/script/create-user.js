db.adminCommand(
  {
    createUser: "admin",
    pwd: "smartapp#123",
    roles: [ "root" ]
  }
);

db.auth("admin","smartapp#123");
db.adminCommand(
  {
    createUser: "smartapp",
    pwd: "smartapp#123",
    roles: [ "readWriteAnyDatabase", "userAdminAnyDatabase", "dbAdminAnyDatabase" ]
  }
);
