SmartPlatform Setup
==========================================
![Fourcicle](https://static.wixstatic.com/media/53ed29_939de150e76c449b9fe6b1eca52457d7~mv2.png/v1/fill/w_159,h_89,al_c,q_80,usm_0.66_1.00_0.01/fourcicle_uhd-01-02.webp)

# Projeto
Este projeto consiste em disponibilizar o setup de instalação da platforma smartplatform.

# Procedimentos
Consulte no WIKI wiki.smartplatform.com.br os procedimentos atualizados para realizar o setup e instalação da plataforma.

# Referências:
- https://www.fourcicle.com.br

# Créditos
- Criado por: [Ely Gonçalves](https://www.fourcicle.com.br)
- Criado em: 28/03/2020 23:33
- Atualizado em: 02/05/2020 22:28
