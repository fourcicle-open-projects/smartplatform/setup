conn = new Mongo();
dbU = conn.getDB("api_umbrella");

dbU.createUser(
  {
    user: "api_umbrella",
    pwd: "smartapp#123",
    roles: [
      { role: "readWrite", db: "api_umbrella" },
      { role: "dbAdmin", db: "api_umbrella" },
    ]
  }
);

db.adminCommand(
  {
    createUser: "admin",
    pwd: "smartapp#123",
    roles: [ "root" ]
  }
);

db.auth("admin","smartapp#123");

db.adminCommand(
  {
    createUser: "smartapp",
    pwd: "smartapp#123",
    roles: [ "readWriteAnyDatabase", "userAdminAnyDatabase", "dbAdminAnyDatabase" ]
  }
);
