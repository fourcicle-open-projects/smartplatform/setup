###############################################################################
# Script________: Makefile
# Função________: Gerenciamento do Docker
# Criado por____: Ely Gonçalves
# Criado em_____: 16/04/2020 00:13
# Atualizado por: Ely Gonçalves
# Atualizado em_: 17/04/2020 14:16
###############################################################################

# Declaração das variáveis
APP_NAME=fourcicle/smart-rules-objeto
VERSION=latest
DEFAULT_PORT=5000

# Scripts
run:
	@echo 'Executando o container $(APP_NAME)'
	docker run -i -t --rm -p=$(DEFAULT_PORT):$(DEFAULT_PORT) --name="$(APP_NAME)" $(APP_NAME)

up: down rm-latest
	@echo 'Subindo o container $(APP_NAME)'
	docker-compose up -d
	docker-compose ps
	docker-compose logs -f -t

down:
	@echo 'Baixando o container $(APP_NAME)'
	docker-compose down

stop:
	@echo 'Parando o container $(APP_NAME)'
	docker stop $(APP_NAME)

rm-latest:
	@echo 'Apagando a imagem $(APP_NAME):$(VERSION)'
	docker image rm $(APP_NAME):$(VERSION)

log:
	@echo 'Exibindo os logs do container $(APP_NAME)'
	docker-compose logs -f -t

ps:
	@echo 'Listando os composes em execução'
	docker-compose ps

help:
	@echo "Exemplos de utilização dos comandos"
	@echo ""
	@echo "Comando    | Função"
	@echo "-------------------"
	@echo "make up    | Subir o container"
	@echo "make down  | Baixar o container"
	@echo "make log   | Listar o log do container"
	@echo ""
	@echo "Maiores informações consulte o arquivo Makefile"
